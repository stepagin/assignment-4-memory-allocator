#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... ); // ok
void debug(const char* fmt, ... ); // ok

extern inline block_size size_from_capacity( block_capacity cap ); // ok
extern inline block_capacity capacity_from_size( block_size sz ); // ok

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; } // ok
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); } // ok
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; } // ok

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) { // ok
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); } // ok

extern inline bool region_is_invalid( const struct region* r ); // ok



static void* map_pages(void const* addr, size_t length, int additional_flags) { // ok
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) { // ok
  query = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
  void* map_addr = map_pages(addr, query, MAP_FIXED_NOREPLACE);
  if (map_addr == MAP_FAILED || map_addr == NULL)
      map_addr = map_pages(addr, query, 0);
  if (map_addr == MAP_FAILED || map_addr == NULL)
      return REGION_INVALID;
  struct region const region = (struct region){.addr = map_addr,
                                .extends = map_addr == addr,
                                .size = query};
								
  block_init(map_addr, (block_size){region.size}, NULL);
  return region;

}

static void* block_after( struct block_header const* block )         ; // ok

void* heap_init( size_t initial ) { // ok
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) { // ok
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) { // ok
  if ( !block_splittable(block, query)) 
	  return false;
  
  void* ad = block->contents + query;
  
  block_init( ad, (block_size) { 
	block->capacity.bytes - query 
  }, block->next);
  
  block->capacity.bytes = query;
  
  block->next = ad;
  
  return true;

}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              { // ok
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous ( // ok
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) { // ok
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) { // ok
    if ( block->next == NULL || !mergeable(block, block->next) ) 
		return false;
    block->capacity.bytes += size_from_capacity( block->next->capacity).bytes;
    block->next = block->next->next;
    return true;

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result { // ok
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    { // ok
    if (!block) {
        return (struct block_search_result){
            .type = BSR_CORRUPTED,
            .block = block,
        };
    }
	
  while (block != NULL) {
    if (block->is_free) {
      while (try_merge_with_next(block));
	  
      if (block_is_big_enough(sz, block))
          return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK,
                                              .block = block};
    }
    if (!block->next) 
		break;
	
    block = block->next;
  }
    return (struct block_search_result){
        .type = BSR_REACHED_END_NOT_FOUND,
        .block = block,
    };


}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) { // ok
  struct block_search_result const search_result = find_good_or_last(block, query);
  
  if (search_result.type != BSR_FOUND_GOOD_BLOCK) 
	  return search_result;
  
  split_if_too_big(search_result.block, query);
  
  search_result.block->is_free = false;
  return search_result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) { // ok
    if (last == NULL) return NULL;
	struct region region = alloc_region(block_after(last), query);
	if (region_is_invalid(&region)) 
		return NULL;

    struct block_header *address = region.addr;
    last->next = address;
    if (try_merge_with_next(last)) 
		return last;
    else 
		return address;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) { // ok
    if (BLOCK_MIN_CAPACITY > query) query = BLOCK_MIN_CAPACITY;
    struct block_search_result searched_block = try_memalloc_existing(query, heap_start);
	
    while (searched_block.type == BSR_REACHED_END_NOT_FOUND) {
        heap_start = grow_heap(searched_block.block, query);
        searched_block = try_memalloc_existing(query, heap_start);
    }
	
    if (searched_block.type == BSR_FOUND_GOOD_BLOCK) 
		return searched_block.block;
    else 
		return NULL;

}

void* _malloc( size_t query ) { // ok
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) { // ok
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) { // ok
  if (!mem) 
	  return;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));

}
